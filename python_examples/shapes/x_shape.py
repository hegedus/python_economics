
# needs to be odd number
a = 11
for i in range(int(a/2)):
    # print(i)
    # "some"[2] = '*'
    print((" " * i) + "*" + (" " * ((a - (2*i)) - 2)) + "*" + (" " * i))

print((" " * int(a/2)) + "*" + (" " * int(a/2)))

for i in range(int(a/2)-1, -1, -1):
    # print(i)
    print((" " * i) + "*" + (" " * ((a - (2*i)) - 2)) + "*" + (" " * i))
