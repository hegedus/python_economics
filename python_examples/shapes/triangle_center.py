
# needs to be odd number
# w = 25
length = int(input("How long should be a triangle: "))

for i in range(1, length + 2, 2):
    count_of_spaces_on_side = int((length - i) / 2)
    print((" " * count_of_spaces_on_side) + ("*" * i) + (" " * count_of_spaces_on_side))
