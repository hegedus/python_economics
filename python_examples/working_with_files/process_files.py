
article = '''
Karlův most (německy die Karlsbrücke, původně Kamenný most) je nejstarší stojící most přes řeku Vltavu v Praze a druhý nejstarší dochovaný most v České republice. Po pražském mostě Juditině, kamenném mostě v Písku a mostě v Roudnici nad Labem jde v Čechách v pořadí o čtvrtou kamennou mostní stavbu.

Karlův most nahradil předchozí Juditin most, stržený roku 1342 při jarním tání ledů. Stavba nového mostu začala v roce 1357 pod záštitou krále Karla IV. a byla dokončena v roce 1402. Praha se i díky kamennému mostu stala významnou zastávkou na evropských obchodních stezkách.

Od konce 17. století bylo na most postupně umístěno 30 převážně barokních soch a sousoší. Původně se mu říkalo jen „Kamenný“ nebo „Pražský“. Název „Karlův most“ se vžil až kolem roku 1870; jako první toto označení použil pražský nakladatel, spisovatel a mědirytec Joseph Rudl v monografii s názvem Die Berühmte Karls-Brücke und ihre Statuen, mit einem kurzen Anhange: Die Franzens-Ketten-Brücke.
'''

file = open('karlov_most.txt', 'w')

file.write(article)
file.close()

file.__enter__()
file.__exit__()

with open('karlov_most.txt', 'w') as file:
    # f.write("\n".join([", ".join(data_line) for data_line in my_data]))
    file.write(article)


with open('karlov_most.txt', 'r') as f:
    data_from_file = f.read()


print("data in file are")
print(data_from_file)
