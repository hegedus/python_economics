# file = open('value_added_data.csv')
# content_of_a_file = file.read()
#
# type(content_of_a_file)
# content_of_a_file
#
# 'a b bsome c bsome'.find('bsome')
#
# index = content_of_a_file.find('2008')
# content_of_a_file[index:index+4]
# content_of_a_file
# dir(content_of_a_file)
#
# content_of_a_file.split(',')
# content_of_a_file.split('2')
#
# len(content_of_a_file)
# len(content_of_a_file.split(','))
#
#
# content_of_a_file.isnumeric()
# content_of_a_file[index:index+4].isnumeric()
#
# help(content_of_a_file.isnumeric)
# help(content_of_a_file.split)
#
# splited_content = content_of_a_file.split(',')
# dir(splited_content)
# help(splited_content.index)
# index=splited_content.index('2008')
# splited_content[index]
#
# content_of_a_file.split('\n')

#
# our_formated_data = []
# for row in content_of_a_file.split('\n'):
#     our_formated_data.append(row.split(','))

# [row.split(',') for row in content_of_a_file.split('\n')]

# [i*i for i in range(5, 20)]


# for i in list(range(10)):
#     print(i)
#
# for formated_row in our_formated_data:
#     print(formated_row)
#     print(formated_row[2])
#
# va_file_string
# va_file_content

#
# Open csv file and store it list of lists
#

import csv

with open('value_added_data.csv') as f:
    # va_file_string = f.read()
    # va_file_string = f.readlines()
    va_file_content = list(csv.reader(f))


va_file_content[3]
va_file_content_by_columns[3]


va_file_content[3][4] + "my_string"
va_file_content[3][4]
i = 14

va_file_content_by_columns = []

def give_me_a_column(index_to_take, list_from_to_take):
    va_file_content_column = []
    for row in list_from_to_take:
        # print(row[i])
        va_file_content_column.append(row[index_to_take])
    return va_file_content_column


count_of_columns = len(va_file_content[0])
va_file_content_by_columns = []
for i in list(range(count_of_columns)):
    # print(give_me_a_column(i, va_file_content))
    va_file_content_by_columns.append(give_me_a_column(i, va_file_content))


va_file_content_by_columns[0]

    # for data_item in row:
    #     print(data_item)

    va_file_content_by_columns.append(row[7])

print(va_file_content_by_columns)


count_of_columns

i = 0
while True:
    print(give_me_a_column(i, va_file_content))
    i += 1
    if i == count_of_columns:
        break

#
# Select column for year 2005
#

col_2005 = [row[2] for row in va_file_content]

#
# Make sum of VA in year 2005
#

# 1. way
sum_2005 = 0
# sum_2005 = '0'
for val in col_2005[2:]:
    if val != '':  # if value is not empty (empty string)
        sum_2005 = sum_2005 + int(val)
        # sum_2005 = sum_2005 + val
print(sum_2005)

# 2.way
sum([int(i) for i in col_2005[2:] if i != ''])


# min([int(i) for i in col_2005[2:] if i.isnumeric()])
# max([int(i) for i in col_2005[2:] if i.isnumeric()])

#
# Reformat data
#

#
# Reformat the data to have
# [
#     (
#        industry,
#        [
#             (year, quorter, value),
#             ...
#         ]
#     ),
#     ...
# ]

# separate year, qoaters and the data (from the csv)
headers = va_file_content[:2]
years = [i for i in headers[0][1:] if i != '']
quoaters = headers[1][2:]
data = va_file_content[2:]

# Go through the data row by row and collect them in the requested format

formated_data = []
# row = data[0]  # This line is if I want to simulate first cycle of for loop
# the first row looks like this
# ['1','Grossdomesticproduct','12761337','12910022','13142873','13332316',...
for row in data:
    # line = row[0]
    industry = row[1]  # this is one of the things we want in the formated data

    # collect [(year, quorter, val)](=formated_vals)
    vals_list = row[2:]  # take just values of the row
    formated_vals = []
    year_idx = 0
    for i, val in enumerate(vals_list):
        # year: year is the same for couples of 4 values in the data.
        #       if we reach the IV qorter, we will add +1 to the index of the year
        # quortes: list of quorters and list of values of the row are correlating
        #          (1-st item in values, is 1-st item in quorters list)
        # val: it's from the loop definition
        formated_vals.append((years[year_idx], quoaters[i], val))
        if quoaters[i] == 'IV':
            year_idx += 1

    formated_data.append((industry, formated_vals))

formated_data

#
# Second quorter of each year per industry:
#

# 1. way
second_quorters_data = []
for one_quoter_data in formated_data[0][1]:
    if one_quoter_data[1] == 'II':
        second_quorters_data.append(one_quoter_data)

# 2. way
second_quorters_data = []
for index in range(1, len(formated_data[0][1]), 4):
    second_quorters_data.append(formated_data[0][1][index])

# 3. way
list(filter(lambda one_quoter_data: one_quoter_data[1] == 'II', formated_data[0][1]))

#
# Values of year 2010
#

state_2010 = []
# row = formated_data[0];
for row in formated_data:
    state_2010.append(
        (
            row[0],
            list(filter(lambda t: t[0] == '2010', row[1]))
        )
    )
state_2010

# lambda t: t[0] == '2010'
#
# def is_2010(t):
#     return t[0] == '2010'

#
# Sums by industry
#
sums_by_industry = []
for row in formated_data:
    # NOTE: explain list comrehension, if not yet explained
    summed_va = sum([int(item[2]) if item[2] != '' else 0 for item in row[1]])
    sums_by_industry.append((row[0], summed_va))


# Starting from the va_file_content. Calculate average value_added by industry
#

def calculate_the_average(list_to_calculate):
    return sum(list_to_calculate) / len(list_to_calculate)


avg_by_industry = []
for row in formated_data:
    # NOTE: explain list comrehension, if not yet explained
    # NOTE: explain ternary operator, if not yet explained
    avg_va = calculate_the_average([int(item[2]) if item[2] != '' else 0 for item in row[1]])
    avg_by_industry.append((row[0], avg_va))


#
# Sums by industry and a by year
#
# Result datastructure:
# [
#     (name_of_industry: [
#         (year, sum_of_the_year),
#         ...
#     ])
#     ...
# ]

sums_by_industry_by_year = []
row = formated_data[0]
for row in formated_data:
    # NOTE: explain list comrehension, if not yet explained
    row_years_data = row[1]
    for i in range(0, len(row_years_data), 4):
        year_data = row_years_data[i:i+4]
        summed_va_for_year = sum([int(item[2]) if item[2] != '' else 0 for item in year_data])

    sums_by_industry_by_year.append((row[0], summed_va_for_year))


#
# How to go through list by "couples of 4"
# Or: How to group data every 4 values
#

data = [
    ('2005', 'I', 1),
    ('2005', 'II', 1),
    ('2005', 'III', 1),
    ('2005', 'IV', 1),
    ('2006', 'I', 1),
    ('2006', 'II', 1),
    ('2006', 'III', 1),
    ('2006', 'IV', 1),
]

i = 0
data_grouped = []
group = []
for row in data:
    group.append(row)
    if i == 3:
        data_grouped.append(group)
        group = []
        i = 0
    i += 1


years
data_grouped = []
for year in years:
    # 1. way
    # group = []
    # for row in data:
    #     if row[0] == year:
    #         group.append(row)
    # 2. way
    group = [row for row in data if row[0] == year]
    data_grouped.append(group)






#
# Sums and averages by industry and a by year
#




####

#
# sums by quartal
# we know the groups in advice
#

# more general group-by (when we don't know all the groups in advice) using default dict
from collections import defaultdict  # noqa
vas_grouped_by_quartal = defaultdict(list)
for row in formated_data:
    for data in row[1]:
        vas_grouped_by_quartal[data[1]].append(int(data[2]) if data[2] != '' else 0)
vas_grouped_by_quartal

sums_by_quartal = []
for quartal, data in vas_grouped_by_quartal.items():
    sums_by_quartal.append((quartal, sum(data)))
sums_by_quartal


# Transpose a list of lists (From list of rows, make list of columns)

l = [
    ['row0', 1, 2, 'col3', 4],
    ['row1', 3, 4, 'col3', 6],
    ['row2', 3, 7, 'col3', 8],
    ['row3', 3, 4, 'col3', 6]
]

list_of_cols = [[] for i in l[0]]
for row in l:
    col_idx = 0
    for col in row:  # NOTE: you can use enumerate here instead of col_idx
        list_of_cols[col_idx].append(col)
        col_idx += 1
list_of_cols








#
