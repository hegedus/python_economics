#Data Types

#
# Boolean
#

our_truth_value = True

print(our_truth_value)

type(True)

boolean`

a = True
b = False
a and b
(a and b) or c
a or b

(True and False) or True

# Tabuľka pravdivostných hodnôt operácie a AND b
AND
a b result
0 0 0
1 0 0
0 1 0
1 1 1

# Tabuľka pravdivostných hodnôt operácie a AND b
OR
a b result
0 0 0
1 0 1
0 1 1
1 1 1

# operátory porovnania
1 == 0
1 != 0
1 > 0
1 < 0
1 >= 0
1 <= 0
"text" == "text"
"text1" != "text2"


#
# Numbers: Integer and Float
#

1  # Integer (Celé číslo)
2.5  # Float (Racionálne číslo)
print(type(1))
print(type(2.5))
print(type("s"))

# Texts: String
"some string value"
print(type("some string value"))

"text1" + " Text2" + " text3"
"text{} Text{} text{}".format(1, 2, 3)

# Lists of items

['val1', 'val2']
print(type(['val1', 'val2']))

# Dictionaries

{
    'key1': 1,
    'key2': "val2"
}
print(type(
    {
        'key1': 1,
        'key2': "val2"
    }
))

# get from list
my_list = [1,2,3,4,5,6,7,8,9]
my_list[3]
l
# by index
l[3]

[1,2,3,4,5,6,7,8,9][3]
# by slice
l[0:7]
l[5:len(l)]

l[-1]

t = (1,2,3,4,5,6)
t[3]

l = [1, 2, 3, 4]
l[1]
sorted(l, reverse=True)
sorted(d)
list(d)
d
d['b']
# get from dict
d = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
# add to list and dict
d['c']
d.keys()
d.items()
for k, v in d.items():
    print(k, v)

# get items for list of keys
ks = ['a', 'c']
[d[key] for key in ks]
list(map(d.get, ks))
from operator import itemgetter
itemgetter(*ks)(d)


set([1,2,3,4,5,5,4,3])

set([1,2,3,4,5,5,4,3]) & set([1,2,3, 7])

"obj".__eq__

class my_class:
    a = None

    def __init__(self, a):
        self.a = a

    def __eq__(self, value):
        print("Porovnavam")
        return self.a == value.a

    def __hash__(self):
        return self.a.__hash__()

    def __repr__(self):
        return "my_class with attribure: {}".format(self.a)
(1).__eq__
dir(1)

obj1 = my_class(1)
obj2 = my_class(1)
obj3 = my_class(2)

obj1 == obj2

set([obj1, obj2, obj3])
set([1,1,2])

sorted()

class my_class_without_eq:
    a = None

    def __init__(self, a):
        self.a = a

o1 = my_class_without_eq(1)
o2 = my_class_without_eq(1)

o1 == o1
o1 == o2

set([o1, o2])


# delete from list and dict
del l[2]
l
del d['a']
d

#
# List comprehension
#
"""
List comprehension is a way of defining a new list by for cycle
"""
# let say we have the list like this
my_words = ['text-sadlkf', 'lsadjf-lkasf', 'ldsakjf-ksdafj']

#  and we want a new list with replaced dash ('-') in the words with underscore ('_')

# one way is like this
new_list_of_words = []
for prvok in moj_zoznam:
    new_list_of_words.append(prvok.replace('-', '_'))

# but if we want to do this in the "Definition of the list - using [] brackets, we can do it like this"
# this is often used as it is clear for everyone that by puttion code into [] branckets, we want to 'create a new list'
# (using 'for' cycle is more general approach.
#    because 'for' is often used for lot of other things then just creating a list)
# This method of creating the list is called "List Comprehension"
new_list_of_words = [prvok.replace('-', '_')
        for prvok in my_words
]
print(new_list_of_words)

#
# Dict Comprehension
#
"""
It is similar to List Comprehension" but we define 'key: value' pairs of the dictionary
"""
my_data = [('a-dsaf', 1), ('b-dsaf', 2)]
{prvok[0].replace('-', '_'): prvok[1] for prvok in moje_data}


# Date and time
from datetime import datetime
datetime.now()

teraz = datetime.now()

teraz.month
teraz.month = 5

datetime(year=teraz.year, month=teraz.month+1, day=teraz.day)

from datetime import timedelta

rozdiel = timedelta(days=50)
teraz + rozdiel
teraz - rozdiel

datetime.now().strftime("%d.%m.%Y")

# String format (fulfilling the template)
today = datetime.now().strftime("%d.%m.%Y")
temperature = 17
place = 'Prague'
string_template = "Hello, Today {dnes} is {teplota}° in {miesto}. In {miesto}"

print(string_template.format(dnes=today, teplota=temperature, miesto=place))

#
# Iterables
#
"""
Iterable objects are objects which provides the iterator as the return value of .__iter__() method.
The Iterator is on abject providing the method .__next__() which always returns some "next" item.

Iterable objects can be used in 'for' loop.
Most used iterable object if Python's build-in datatype 'List'
"""
my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]

print('\n'.join(dir(my_list)))

iterator_of_my_list = my_list.__iter__()
print(iterator_of_my_list.__next__())

# the method __next__ if internal function (we can see it via the underscores)
# in normal programming we try to avoid direct call of internal functions.
# to access the next item, there is a build-in function 'next'
print(next(iterator_of_my_list))

for number in my_list:
    print(number)

#
# Generators
#
"""
Generator is Iterable object.
It's simillar to list, but it's difference is that the Generator doen't keep all the item in the Computer's memory.
Generator just generates 'next' objects when it is asked for and the 'previous' ones it throws away.
It's often used for working with large datasets, which would not fit into memory.
"""

# On of the functions which returns generator is 'product'
from itertools import product

variations_of_characters = product("ABCDEFG", repeat=3)
print(variations_of_characters)

# this will transfer generator to the list
# list is datatype which keeps all it's item in the memory
list_of_variations = list(kombinacie)

# as list has all items already in memory, it's easy to access it's items
list_of_variations[3]
list_of_variations[10:20]  # the set of items(slice) of the list

# to access the items of the generator
# we need to use helper functions (which will iterate over a generator for us)
# / or we can iterate over generator by ourselves, eg. using 'for' cycle

from itertools import islice
help(islice)
list(islice(variations_of_characters, 3, 4))
list(islice(variations_of_characters, 10, 20))

my_gen(1,2, count=4)
my_gen()
def my_gen(a='default'):
    print(a)

# Own Generator
def my_gen(n, i, count=None):
    for i in range(n):
        yield i


print(list(my_gen(7)))




#
