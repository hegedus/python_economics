from urllib.request import urlopen
from bs4 import BeautifulSoup
import requests
from selenium import webdriver

#
# Collect data from jobs.cz
#
jobs_cz_base_url = 'https://www.jobs.cz'


# # First page of our interest
# url_jobs_cz = 'https://www.jobs.cz/en/?locality[code]=R200000&locality[label]=Prague&locality[coords]=50.08455%2C14.41778&locality[radius]=50'
def collect_data_for_jobs_cz_page(url_jobs_cz):
    server_response = requests.get(url_jobs_cz)
    # dir(server_response)
    # server_response.text

    # Creat BS Object
    jobs_content_bs = BeautifulSoup(server_response.text)

    # The way how to extract elements of the web page by value in attribute, using BSObject.select method
    content_part_div = jobs_content_bs.select("div[class~=content]")[0]

    # Create the list of all company names on web page
    list_of_companies_divs = content_part_div.select("div[class~=search-list__main-info__company]")
    # content_part_div.select("div[class~=search-list__main-info__company]")[0].text.strip()  # The value extraction for one element

    # Collecting the value of all elements
    list_of_companies = []
    for company_div in list_of_companies_divs:
        list_of_companies.append(company_div.text.strip())

    # Create the list of all company adresses on web page
    # content_part_div.select('div[class~=search-list__main-info__address]')[0].select('span')[1].text.strip()  # The value extraction for one element

    # Collecting the value of all elements
    list_of_locations = []
    for location_div in content_part_div.select('div[class~=search-list__main-info__address]'):
        list_of_locations.append(location_div.select('span')[1].text.strip())

    # Create the list of all company free positions on web page
    content_part_div.select('a[class~=search-list__main-info__title__link]')[0].text.strip()  # The value extraction for one element
    list_of_positions = []
    for position_a in content_part_div.select('a[class~=search-list__main-info__title__link]'):
        list_of_positions.append(position_a.text.strip())

    assert len(set([len(l) for l in [list_of_companies, list_of_locations, list_of_positions]])) == 1

    # Put all data of one page into one list
    first_page_data = []
    for i in range(len(list_of_companies)):
        first_page_data.append((
            list_of_companies[i],
            list_of_locations[i],
            list_of_positions[i]
        ))

    return first_page_data

collect_data_for_jobs_cz_page('https://www.jobs.cz/en/?locality[code]=R200000&locality[label]=Prague&locality[coords]=50.08455%2C14.41778&locality[radius]=50')


# Collect data for all pages

# Collect all links of the pages (notice that each page contains only 5 neares page links)

# Link by link

# content_part_div.select('a[class~=pager__item]')[0]['href']  # The value extraction for one element
# Create the list of page urls on web page
list_of_links_paths = []
for a_el_bs in content_part_div.select('a[class~=pager__item]'):
    list_of_links_paths.append(a_el_bs['href'])

#jobs_cz_base_url + list_of_links_paths[0]  # < - this is example for first element. Make it for whole list
list_of_urls = []
for path in list_of_links_paths:
    list_of_urls.append(jobs_cz_base_url + path)

while True:
    print("Accessing: {}".format(list_of_urls[-1]))
    server_response = requests.get(list_of_urls[-1])
    page_content_bs = BeautifulSoup(server_response.text)
    a_els_on_page = page_content_bs.select('a[class~=pager__item]')

    list_of_urls.append(jobs_cz_base_url + a_els_on_page[-2]['href'])
    list_of_urls.append(jobs_cz_base_url + a_els_on_page[-1]['href'])

    if len(page_content_bs.select('a[class~=pager__next]')) == 0:
        break



# remove duplicated urls in the list (at the end there are some)
list_of_urls_without_duplicates = []
for url in list_of_urls:
    if url not in list_of_urls_without_duplicates:
        list_of_urls_without_duplicates.append(url)


# Collect data about companies for each page

all_jobs_cz_search_query_data = []
for page_url in list_of_urls_without_duplicates:
    print("Accessing: {}".format(page_url))
    all_jobs_cz_search_query_data.append(collect_data_for_jobs_cz_page(page_url))








#
# Collect data from profesia.cz
#

url_prague = 'https://www.profesia.cz/prace/praha/'
response = requests.get(url_prague)

prg = BeautifulSoup(response.content)

prg.find_all(class_='panel')
position_rows = prg.find(id='content').select('.list-row')

positions_companies = []
for row in position_rows:
    position = row.find('span', class_='title').string
    company = row.find('span', class_='employer').string
    positions_companies.append((position, company))


#
# Collect data from berlinstartupjobs
#

base_url = 'https://berlinstartupjobs.com/'
category_url = 'https://berlinstartupjobs.com/engineering/'

html = urlopen(category_url)
response = requests.get(category_url)
soup = BeautifulSoup(html, 'lxml')
type(soup)

driver = webdriver.Chrome()
# driver = webdriver.Firefox()

driver.get(category_url)

dir(driver)

bsj_bs = BeautifulSoup(driver.page_source)
bsj_bs.find_all('div')

#
